#!/bin/bash -ex

# pipenv install --dev
# pipenv install --trusted-host pypi.python.org -r concoursegit/requirements.txt
# cd concoursegit
# pipenv run pytest

git clone https://gitlab.com/Tembhum/concoursedemo.git
cd concoursedemo
pip install -r requirements.txt
pytest